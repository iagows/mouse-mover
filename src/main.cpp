#include <stdio.h>
#include <X11/Xlib.h>

#include <chrono>
#include <thread>

int main() 
{
    using namespace std::chrono_literals; // ns, us, ms, s, h, etc.
    Display *dpy;
    dpy = XOpenDisplay(0);

    int x = 1;
    int y = 1;
    
    while(true){
        XWarpPointer(dpy, None, None, 0, 0, 0, 0, x, y);
        XFlush(dpy);
        
        std::this_thread::sleep_for(10ms);
    }
    return 0;
}
